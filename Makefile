.PHONY: install

install:
	install -d "${DESTDIR}${PREFIX}/bin"
	install -m 755 ocrf "${DESTDIR}${PREFIX}/bin/ocrf"
	install -d "${DESTDIR}${PREFIX}/share/man/man1"
	install -m 644 ocrf.1 "${DESTDIR}${PREFIX}/share/man/man1/ocrf.1"
